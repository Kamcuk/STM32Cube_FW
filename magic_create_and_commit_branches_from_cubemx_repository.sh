#!/bin/bash
set -euo pipefail

repository=~/STM32Cube/Repository

# Functions ##########################################################

debug() {  if ${DEBUG:-true}; then echo "$@"; fi }
warn() { echo "WARN:" "$@" >&2; }
fatal() { echo "FATAL:" "$@" >&2; exit 2; }
ask_ok() { if ${ASK_OK:-true}; then read -p "All ok?"; fi; }
usage() {
	cat <<EOF
Usage: $(basename $0) [stm32cube repository folder]

Default STM32cube repository folder is $repository.
EOF
}
git_checkout_or_orphan() {
	if [ -n "$(git branch --list "$1")" ]; then
		echo "Checking out $1..."
		git checkout "$1"
		echo "Removing untracked files..."
	else
		echo "Creating orphan branch $s with repository content..."
		git checkout --orphan "$1"
	fi
	echo "Resetting and removing untracked files..." 
	git reset --hard
	git clean -fdx
}
git_symbolic_ref() {
	git symbolic-ref HEAD refs/heads/"$1"
	git reset
}
git_symbolic_ref_or_orphan() {
	if [ -n "$(git branch --list "$1")" ]; then
		echo "Symbolic-ref to $1..."
		git_symbolic_ref "$1"
	else
		echo "Creating orphan branch $s with repository content..."
		git checkout --orphan "$1"
	fi
}
git_tag_or_not() {
	if git rev-parse -q --verify "refs/tags/$1" >/dev/null; then
		echo "Tag $1 already exists..."
	else
		echo "Creating git tag $1 ..."
		git tag "$1"
	fi
}
git_add_all_commit_or_not() {
	echo "git reset && git add -A . ..."
	git reset
	git add -A .
	if [ -n "$(git status -s)" ]; then
		echo "Adding and commiting..."
		git commit -m "$@"
	else
		echo "Nothings changed..."
	fi
}
move_git() {
	declare -g move_git_from
	move_git_from=$(readlink -f $(pwd))
	declare -g move_git_to
	move_git_to=$(readlink -f $1)
	echo "Moving .git folder to $move_git_to ..."
	mv -v .git "$move_git_to"
	_exit() {
		set -x ||:
		echo ||:
		echo "Errror in script. Invoked EXIT handler..." ||:
		mv -v "$move_git_to"/.git "$move_git_from" ||:
		cd "$move_git_from" ||:
		git_symbolic_ref master ||:
	}
	trap '_exit' EXIT
	pushd "$path" >/dev/null
	ask_ok
}
move_git_back() {
	echo "Moving back .git folder to $move_git_from ..."
	mv -v "$move_git_to"/.git "$move_git_from"
	popd >/dev/null
	git_symbolic_ref master
	trap '' EXIT
	ask_ok
}
generate_gitignore_use() {
	echo "Generting .gitignore for *_use branch..."
	{
	find . -type f ! -path '*/\.*' \
		-a ! -name '*\.c' -a ! -name '*\.h' \
		-a ! -name '*\.s' -a ! -name '*\.S' \
		-a ! -name '*\.asm' -a ! -name '*\.ld' \
	| sed 's/^.*\./*./' | sort -u 
	printf "%s\n" .gitignore Documentation Drivers/BSP Projects Utilities _htmresc
	find Drivers Middlewares -name Release_Notes.html -o -name 'license.txt' | sed 's/^/!/'
	} > .gitignore
}

# Main ###########################################################

if [ "$(dirname "$0")" != "/tmp" ]; then
	echo "Not running from tmp directory. Copying itself and restarting..."
	d="/tmp/$(basename $0)"
	cp -v "$0" "$d"
	chmod +x "$d"
	exec "$d" "$@"
fi
if ! git status -s >/dev/null; then
	fatal "Not in a git repo"
fi
if [ -n "$(git status -s)" ]; then
	fatal "Master branch should be commited and clean before running this script."
fi

repository=${1:-$repository}
repository=$(readlink -f "$repository")
declare -r repository

if [ ! -d "$repository" ]; then
	usage
	fatal "$repository directory does not exists"
fi
echo "Using stm32cubeMX repository from $repository"

series=$(printf "%s\n" "$repository"/STM32Cube_FW_* | sed 's;^.*/STM32Cube_FW_\(.*\)_.*$;\1;' \
	| sort -u | tr '\n' ' ')
echo "Series sources found in repository:" $series

echo "Disabling CRLF warning git feature..."
git config core.autocrlf false

for s in $series; do
	path=$(printf "%s\n" "$repository"/STM32Cube_FW_${s}_* | sort -n | tail -n1)
	IFS="_" read _ _ serie ver <<<"$(basename "$path")"

	branch="$serie"
	tag="${serie}_${ver}"
	branch_use="${serie}_use"
	tag_use="${tag}_use"

	move_git "$path"
	generate_gitignore_use
	git_symbolic_ref_or_orphan "${branch_use}"
	git_add_all_commit_or_not "auto-commit for $tag_use" --no-status
	git_tag_or_not "$tag_use"
	rm -v .gitignore

	move_git_back
	git_symbolic_ref_or_orphan master
	if [ -n "$(git status -s)" ]; then fatal "Error: changed on master, something went wrong"; fi

# dont create normal branch-es
if false; then
	git_checkout_or_orphan "$branch"
	echo "Merging branch $branch_use into $branch ..."
	git merge --no-edit "$branch_use"
	git_checkout_or_orphan master
	if [ -n "$(git status -s)" ]; then fatal "Error: changed on master, something went wrong"; fi

	move_git "$path"
	git_symbolic_ref_or_orphan "$branch"
	git_add_all_commit_or_not "auto-commit for $tag" --no-status
	git_tag_or_not "$tag"

	move_git_back
	git_symbolic_ref_or_orphan master
	if [ -n "$(git status -s)" ]; then fatal "Error: changed on master, something went wrong"; fi
fi

done

exit

# old
for s in $series; do
	path=$(printf "%s\n" "$repository"/STM32Cube_FW_${s}_* | sort -n | tail -n1)
	IFS="_" read _ _ s v <<<"$(basename "$path")"



	onlyc="$s-onlyc"
	git_reset_or_orphan "$onlyc"
	echo "Generating .gitignore file..."
	find . -type f ! -path '*/\.*' \
		-a ! -name '*\.c' -a ! -name '*\.h' \
		-a ! -name '*\.s' -a ! -name '*\.S' \
		-a ! -name '*\.asm' -a ! -name '*\.ld' \
	| sed 's/^.*\./*./' | sort -u > .gitignore
	git rm -r --cached . | wc -l
	git_all_commit_or_not "auto-commit for $s-$v-onlyc"
	echo "Removing .gitignore file..."
	rm .gitignore

	git_reset_or_orphan "$s"
	git_all_commit_or_not "auto-commit for $s-$v"

	echo "Moving .git folder back..."
	trap '' EXIT
	mv .git "$dir"
	popd >/dev/null
	git_soft_checkout master
	if [ -n "$(git status -s)" ]; then fatal "Nothing should have changed in master"; fi;
done

exit

# using rsync copy, way slower
for s in $series; do
	path=$(printf "%s\n" "$repository"/STM32Cube_FW_${s}_* | sort -n | tail -n1)
	IFS="_" read _ _ s v <<<"$(basename "$path")"
	git_checkout_or_orphan "$s"
	echo "Copying repository into the branch..."
	rsync --delete -a "$path"/ ./
	git_all_commit_or_not "auto-commit for $s-$v"

	onlyc="$s-onlyc"
	git_checkout_or_orphan "$onlyc"
	echo "Copying repository into the branch..."
	rsync --delete -a "$path"/ ./
	echo "Generating .gitignore file..."
	find . -type f ! -path '*/\.*' -a ! -name '*\.c' -a ! -name '*\.h' -a ! -name '*\.s' -a ! -name '*\.S' -a ! -name '*\.asm' \
	| sed 's/^.*\./*./' | sort -u > .gitignore
	cat .gitignore
	git_all_commit_or_not "auto-commit for $s-$v-onlyc"

	echo "Going back to master..."
	git_checkout_or_orphan master
done


#!/bin/bash

cd "$(dirname "$(readlink -f "$0")")"

set -xuoe pipefail -e errtrace

echo "== Updating all branches"
git_pull_fetch_all.sh

echo "== Downloading all stm32cubemx repositories for each series into ~/STM32Cube/Repository/ folder"
mkdir -p ~/STM32Cube/Repository/
./download_stm32cubemx_repositories.sh list | cut -f1 | xargs -t -i ./download_stm32cubemx_repositories.sh dw {} ~/STM32Cube/Repository/

echo "== Magically commiting each repo"
yes | ./magic_create_and_commit_branches_from_cubemx_repository.sh ~/STM32Cube/Repository/

echo "== Pushing everything to remote server"
./git_push_all_tags.sh



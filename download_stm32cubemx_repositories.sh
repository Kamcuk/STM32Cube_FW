#!/bin/bash
set -euo pipefail; export SHELLOPTS

CACHEDIR=${CACHEDIR:-/tmp/.$(basename $0)}

# functions

usage() {
	cat <<EOF
Usage: stm32cubemx_haldownload.sh MODE [ARG]...

Modes:
	list              - prints the newest list of hal firwares
	dw F0|F1|F2|... DIR  - download hal firware to specified directory
	help                 - display this test and exit

Examples:
	stm32cubemx_haldownload.sh list
	stm32cubemx_haldownload.sh dw F0 /mydir/

Written by Kamil Cukrowski.
Licensed jointly under Beerware License and MIT License.
EOF
}

getlist() {
	local list0 list1 list2 i

	# download updatets.zip
	local tmp
	tmp=_workdir
	mkdir -p "$tmp"
	pushd "$tmp" >/dev/null
	curl -sS -L -o updaters.zip -C - http://www.st.com/resource/en/utility2/updaters.zip
	unzip -q -o updaters.zip
	list0=$(cat html/STM32*  | sed 's/[<>]/\n/g' | grep '^STM32Cube[LF]')
	popd >/dev/null
	
	# get newest drivers paths/names
	list1=$(echo "$list0" | sed 's/STM32Cube\(..\) Firmware Package V\([0-9]*\)\.\([0-9]*\)\.\([0-9]*\)[[:space:]]*\/[[:space:]]*\(.*\)/\2 \3 \4 stm32cube_fw_\1_v\2\3\4.zip \5/')
	list2=$(cut -d'_' -f2,3 <<<"$list1" | sort -u)
	for i in $list2; do
		tmp=$(grep " stm32cube_$i" <<<"$list1" | sort -n -k1 -k2 -k3 | tail -n1 | cut -d' ' -f4- | tr '[:upper:]' '[:lower:]' | tr ' ' '\t')
		echo "$(<<<"$tmp" cut -d_ -f3)"$'\t'"$tmp"
	done
}

case "${1:-mode}" in
list) getlist; ;;
help) usage; exit 0; ;;
all)
	dest=${2:-$HOME/STM32Cube/Repository}
	tmp=$(getlist | cut -f2)
	for i in $tmp; do
		( set -x;
			cd "$dest"
			curl -L -o $i -C - http://www.st.com/resource/en/firmware2/$i
			unzip -q -o $i
		)
	done
	;;
dw)
	t=$(tr '[:upper:]' '[:lower:]' <<<"$2")
	list=$(getlist | cut -f2)
	if ! i=$(grep ^stm32cube_fw_"$t"_ <<<"$list"); then
		echo "Firmware for serie $1 not found!"
		exit 1
	fi
	dest=${3:-$HOME/STM32Cube/Repository}
	( set -x
		cd "$dest"
		curl -L -o $i -C - http://www.st.com/resource/en/firmware2/$i
		unzip -q -o $i
	)
	;;
*) usage; exit 1; ;;
esac



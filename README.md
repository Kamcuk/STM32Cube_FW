STM32Cube_MX repository
======================

This repository contains in each branch different hal drivers downloaded using STM32Cube_MX.

All software in this repo exept for master branch may be downloaded from st.com site [http://www.st.com/en/embedded-software/stm32cube-mcu-packages.html?querycriteria=productId=LN1897](http://www.st.com/en/embedded-software/stm32cube-mcu-packages.html?querycriteria=productId=LN1897).

Branches
========

There is one branch for each stm32 serie.

Each branch has only compilable files needed to compile programs.

Branches are named F1_use, F2_use, L0_use and so on.

The master branch contains scripts to download files and this readme.

Reason for this repo
====================

Theres no way to download hal drivers from stm32 using batch script, so i needed to create such repo for automatic building.

If there however is a way, that i can automate building stm hal drivers into cmake, please create an issue and i will remove this repo (is i need to update this repo manually).  

How to update this repo
=======================

- Run the git_pull_fetch_all.sh
- Open stm32cubemx
- Let it download the repositories
- Run the magic_* script, answer with enter
- Run the git_push_all_tags.sh

License
=======

All files created by me are licensed joinly under Beerware License and MIT License.

All files created and shipped by STM have their own license.

STM works are licensed under [MCD-ST Liberty SW License Agreement V2](http://www.st.com/software_license_agreement_liberty_v2), see LICENSE.st.txt file.

FreeRTOS is licensed under [FreeRTOS MIT License](https://www.freertos.org/license.txt), see LICENSE.freertos.txt file.

FatFS is licensed to ChaN, license may be found here [license](http://elm-chan.org/fsw/ff/pf/appnote.html), see LICENSE.fatfs.txt file.

Some work is under ARM License.

So do some research under which license is source file distributed before using it.

StemWIN is available [here](http://www.st.com/en/embedded-software/stemwin.html). 

And probably many other license, which i didn't find, please add issue if your offended.

Author
======

Kamil Cukrowski  

